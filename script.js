const circle = document.querySelector('.dwara')
const scoreHTML = document.querySelector('#Score')

var score = 0

function changePosition(){
    var h = document.querySelector('.container').clientHeight;
    var w = document.querySelector('.container').clientWidth;

    // Generating Random Postions
    let randX = Math.floor(Math.random() * w*0.75)
    let randY = Math.floor(Math.random() * h*0.69)

    // Setting Circle Position
    circle.style.left = `${randX}px`
    circle.style.top = `${randY}px`
}

// Change Position every second
const positionInterv = setInterval(()=>{
    changePosition()
}

, 1000)

circle.addEventListener('click', ()=>{
    score += 100
    scoreHTML.innerHTML = score
})